﻿using System.Web.Optimization;

namespace FixMe.Mvc
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                        "~/Scripts/jquery-3.2.1.min.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/owl.carousel.min.js",
                        "~/Scripts/jquery.nicescroll.min.js",
                        "~/Scripts/isotope.pkgd.min.js",
                        "~/Scripts/imagesloaded.pkgd.min.js",
                        "~/Scripts/circle-progress.min.js",
                        "~/Scripts/main.js"));

            //if you find me...then just simply uncomment code below to fix styling issue
            /*
            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/owl.carousel.min.css",
                      "~/Content/animate.css",
                      "~/Content/style.css"));
            */
        }
    }
}
